import pymysql


# 解析 .sql 文件，拆分成逐条语句
# Reference: http://adamlamers.com/post/GRBJUKCDMPOA
def parse_sql(filepath):
    with open(filepath, "r", encoding="gbk") as f:
        data = f.readlines()

    stmts = []
    DELIMITER = ";"
    stmt = ""

    for lineno, line in enumerate(data):
        if not line.strip():
            continue

        if line.startswith("--"):
            continue

        if "DELIMITER" in line:
            DELIMITER = line.split()[1]
            continue

        if (DELIMITER not in line):
            stmt += line.replace(DELIMITER, ";")
            continue

        if stmt:
            stmt += line
            stmts.append(stmt.strip())
            stmt = ""
        else:
            stmts.append(line.strip())
    return stmts


# 初始化数据库
def init_db(filepath):
    # 建立连接
    conn = pymysql.connect(
        host="localhost",
        user="root",
        password="root12345"    # 你的密码
    )

    # 将 .sql 文件拆成逐条语句
    stmts = parse_sql(filepath)
    with conn:
        with conn.cursor() as cursor:
            for stmt in stmts:
                print("### Execute:")
                print(stmt)
                cursor.execute(stmt)
                results = cursor.fetchall()
                # 输出反馈信息
                if results:
                    print("### Results:")
                for row in results:
                    print(row)

            # 必须 commit 数据库改动才会生效
            conn.commit()

    # 前面使用 with 自动管理 conn 和 cursor，此处无需 close


if __name__ == "__main__":
    # .sql 文件，用于初始化数据库内容
    filepath = "schema.sql"
    init_db(filepath)
