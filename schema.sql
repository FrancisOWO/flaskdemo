
-- 建立数据库
drop database if exists test_student;
create database test_student;
use test_student;

-- 建立数据库表
/*
	# 用户表
	username 	用户名（主键）
	password 	密码
*/
create table user (
	username 	varchar(20) primary key not null,
	password 	varchar(256) not null
);

/*
	# 院系表
	no 		院系编号（主键，自增）
	name 	院系名称
*/
create table dept (
	no 		integer primary key auto_increment not null,
	name 	varchar(45) not null
);

/*
	# 学生表
	no 		学号（主键）
	name 	姓名
	gender 	性别
	age 	年龄
	d_no 	院系编号（外键，依赖 dept 表）
*/
create table student (
	no 		integer primary key not null,
	name 	varchar(30) not null,
	gender 	varchar(1) not null check (gender in ('F','M')),
	age 	integer not null,
	d_no 	integer not null, 
	constraint st_c_1 foreign key (d_no) references dept(no)
);

/*
	# 课程表
	no 		课程编号（主键，自增）
	name 	课程名称
	credit 	学分
	d_no 	院系编号（外键，依赖 dept 表）
*/
create table course (
	no 		integer primary key auto_increment not null,
	name 	varchar(60) not null,
	credit 	integer not null,
	d_no 	integer not null,
	constraint co_c_1 foreign key (d_no) references dept(no)
);

/*
	# 成绩表
	s_no 	学生编号（外键，依赖 student 表）
	c_no 	课程编号（外键，依赖 course 表）
	score 	成绩
	log_dt	登记时间（yyyy-MM-dd HH:mm:ss）
*/
create table score (
	s_no 	integer not null,
	c_no 	integer not null,
	score 	integer not null,
	log_dt	datetime not null,
	constraint sc_c_1 foreign key (s_no) references student(no),
	constraint sc_c_2 foreign key (c_no) references course(no)
);

-- 插入数据
insert into dept (no,name) values (1,'计算机系');
insert into dept (no,name) values (2,'数学系');
insert into dept (no,name) values (3,'建筑系');
insert into dept (no,name) values (4,'管理学系');

insert into course (no,name,credit,d_no) values (1,'数据库原理',3,1);
insert into course (no,name,credit,d_no) values (2,'高等数学',5,2);
insert into course (no,name,credit,d_no) values (3,'管理学概论',2,4);
insert into course (no,name,credit,d_no) values (4,'操作系统',4,1);
insert into course (no,name,credit,d_no) values (5,'数据结构',4,1);
insert into course (no,name,credit,d_no) values (6,'概率论',3,2);
insert into course (no,name,credit,d_no) values (7,'Python程序设计',2,1);

insert into student (no,name,gender,age,d_no) values (200215120,'小明','M',21,3);
insert into student (no,name,gender,age,d_no) values (200215121,'大明','M',20,1);
insert into student (no,name,gender,age,d_no) values (200215122,'小红','F',19,1);
insert into student (no,name,gender,age,d_no) values (200215123,'小丽','F',18,2);
insert into student (no,name,gender,age,d_no) values (200215125,'小华','M',19,3);

insert into score (s_no,c_no,score,log_dt) values (200215121,1,92,NOW());
insert into score (s_no,c_no,score,log_dt) values (200215121,2,85,NOW());
insert into score (s_no,c_no,score,log_dt) values (200215121,3,88,NOW());
insert into score (s_no,c_no,score,log_dt) values (200215122,2,90,NOW());
insert into score (s_no,c_no,score,log_dt) values (200215122,3,80,NOW());

-- 查询数据
select * from student;
select * from course;
select * from dept;
select * from score;
