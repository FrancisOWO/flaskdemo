import os
basedir = os.path.abspath(os.path.dirname(__file__))

class Config(object):
    SECRET_KEY = os.environ.get("SECRET_KEY") or "you-will-never-guess"
    # MySQL 配置
    HOSTNAME = "localhost"      # 主机名
    PORT = 3306                 # 端口号
    USERNAME = "root"           # 用户名
    PASSWORD = "root12345"      # 密码
    DATABASE = "test_student"   # 数据库名
    SQLALCHEMY_DATABASE_URI = os.environ.get("DATABASE_URL") or \
        f"mysql+pymysql://{USERNAME}:{PASSWORD}@{HOSTNAME}:{PORT}/{DATABASE}?charset=gbk"

    SQLALCHEMY_TRACK_MODIFICATIONS = False
    MAX_CONTENT_LENGTH = 1 * 1024 * 1024
