# FlaskDemo

## 简介

基于 Flask 的数据库课程作业示例

项目地址：[FrancisOWO / FlaskDemo · GitLab](https://gitlab.com/FrancisOWO/flaskdemo)

前端模板：[Easion Bootstrap Dashboard Template (usebootstrap.com)](https://usebootstrap.com/theme/easion)

## 使用说明

首先，请将 ```init_db.py``` 和 ```config.py```  中的数据库用户密码换成自己的！！！

```bash
# 创建 Python 环境
conda init cmd.exe
conda create -n flask python==3.8.18
conda activate flask
pip install -r requirements.txt
# 初始化数据库
python init_db.py
# 启动程序
python app.py
```

或者，在环境配置完成后，也可以双击 ```init_db.bat``` 来初始化数据库，双击 ```startapp.bat``` 来启动程序

