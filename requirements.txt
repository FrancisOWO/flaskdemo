Flask==2.0.3
Flask-Login==0.6.2
Flask-SQLAlchemy==2.5.1
Flask-WTF==1.2.1
PyMySQL==1.1.0
SQLAlchemy==1.4.0
Werkzeug==2.2.3
WTForms==3.1.0
