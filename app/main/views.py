from flask import render_template, redirect, url_for, flash, request

from app import app, db, g_config
from app.account.models import User
from . import main_bp
from .models import Dept, Student, Course, Score
from .forms import TableActForm, DeleteForm, ContentForm

from datetime import datetime, date


def convert_atb(key, value, default=""):
    if key == "gender":
        return "男" if value == "M" else "女"
    elif key == "option":
        return "是" if value == "Y" else "否"
    elif key == "empty":
        return value if bool(value) else default # "无","待填",……
    return value


def iconvert_atb(key, value):
    value = str(value).strip()  # 删去两侧空白符
    if key == "gender":
        return "M" if value == "男" else "F"
    elif key == "option":
        return "Y" if value == "是" else "N"
    elif key == "date":
        format_str = "%Y-%m-%d"
        if not value: value = datetime.now().strftime(format_str)
        # 注意，外部调用 exec，此处需返回函数语句的字符串形式，而不是函数执行结果
        return f"datetime.strptime('{value}','{format_str}').date()"
    elif key == "datetime":
        format_str = "%Y-%m-%d %H:%M:%S"
        if not value: value = datetime.now().strftime(format_str)
        return f"datetime.strptime('{value}','{format_str}')"
    # 注意，xxx_no 的查询都需要保证 value 对应的记录唯一
    # 否则无法保证查询返回期望的结果，比如，如果学生重名，就只能取到其中一个学生的学号
    elif key == "d_no":
        return Dept.query.filter(Dept.name == value).first().no
    elif key == "s_no":
        return Student.query.filter(Student.name == value).first().no
    elif key == "c_no":
        return Course.query.filter(Course.name == value).first().no
    return value


def check_idx(idx, min, max):
    return isinstance(idx, int) and idx >= min and idx <= max


@main_bp.route("/<tablename>", methods=["GET","POST"])
def tables(tablename):

    # 页面不存在，跳转到主页
    tablelinks = g_config.tablelinks
    if tablename not in tablelinks.keys():
        return redirect(url_for("account.panel"))
    tabletitle = tablelinks[tablename]

    form = TableActForm()

    atbs = [] # 表头（属性）
    records = [] # 内容（记录）
    models_dict = {
        "dept": Dept,
        "student": Student,
        "course": Course,
        "score": Score,
    }

    tabletitle = tablelinks[tablename]
    results = models_dict[tablename].query.all()

    if(tablename == "dept"):
        raw_atbs = ["name"]
        convert_keys = [None]
        atbs = ["编号", "院系名称"]
        #records = [[1,"计算机系"]]
        records = [[
            idx+1,
            res.name,
        ] for idx, res in enumerate(results)]
    elif(tablename == "student"):
        raw_atbs = ["no", "name", "gender", "age", "d_no"]
        convert_keys = [None, None, "gender", None, "d_no"]
        atbs = ["编号", "学号", "姓名", "性别", "年龄", "院系"]
        #records = [[200215120, "小明", "男", 21, "建筑系"]]
        records = [[
            idx+1,
            res.no,
            res.name,
            convert_atb("gender", res.gender),
            res.age,
            # 注意，此处存的是 d_no 但显示的是 name，如果要插入数据，则需要反向转换
            # 但是，必须保证 d_no 与 name 唯一对应，反向转换才成立
            Dept.query.get(int(res.d_no)).name,
        ] for idx, res in enumerate(results)]
    elif(tablename == "course"):
        raw_atbs = ["name", "credit", "d_no"]
        convert_keys = [None, None, "d_no"]
        atbs = ["编号", "课程名称", "学分", "院系"]
        #records = [[1, "数据库原理", 3, "计算机系"]]
        records = [[
            idx+1,
            res.name,
            res.credit,
            Dept.query.get(int(res.d_no)).name,
        ] for idx, res in enumerate(results)]
    elif(tablename == "score"):
        raw_atbs = ["s_no", "c_no", "score", "log_dt"]
        convert_keys = [None, "c_no", None, "datetime"]
        atbs = ["编号", "学号", "课程", "成绩", "登记时间"]
        #records = [[200215121, "大明", "数据库原理", 92, "2023-10-31 00:06:39"]]
        records = [[
            idx+1,
            res.s_no,
            Course.query.get(int(res.c_no)).name,
            res.score,
            convert_atb("datetime", res.log_dt),
        ] for idx, res in enumerate(results)]
    else:
        return redirect(url_for("account.panel"))

    # 添加一条数据，每个属性一个输入框，atbs[1:] 排除第一列的自动编号
    add_forms = [ContentForm() for atb in atbs[1:]]

    # 点击"添加、删除、保存、取消"按钮
    if form.validate_on_submit():
        # 添加记录
        if form.add.data:
            g_config.db_status = 1
        # 删除记录
        elif form.delete.data:
            g_config.db_status = 2
        # 保存更改
        elif form.save.data:
            # flash(f"status: {g_config.db_status}")
            if g_config.db_status == 1:
                form_data = request.form
                # app.logger.info(form_data)

                # 获取前端填写的每一列内容
                # 遍历多重集需指定 multi=True，否则重复的键只输出一次
                contents = [v for k,v in form_data.items(multi=True) if k=="content"]
                app.logger.info(f"{contents=}")

                record = models_dict[tablename]()
                for atb, content, key in zip(raw_atbs, contents, convert_keys):
                    app.logger.info(f"{key}: {type(content)} --> {type(iconvert_atb(key, content))}")
                    # TODO: 直接 exec 比较危险，为 record 建立字典，改成向字典插入数据比较好
                    # record.xxx = yyy
                    if key in ["date", "datetime"]:
                        exec(f"record.{atb} = {iconvert_atb(key, content)}")
                    else:       # 赋值右侧加""，当做字符串
                        exec(f"record.{atb} = '{iconvert_atb(key, content)}'")
                db.session.add(record)

            elif g_config.db_status == 3 and check_idx(g_config.delete_no, 1, len(results)):
                res = results[g_config.delete_no-1]
                db.session.delete(res)
                g_config.delete_no = None

            g_config.db_status = 4

            # 若数据库报错，前端显示报错内容
            try:
                db.session.commit()
                flash(f"保存成功！", category="info")
            except Exception as e:
                app.logger.info(f"保存失败: {e}")
                db.session.rollback()
                flash(f"保存失败: {e}", category="danger")

            # 数据库已改动，刷新页面更新数据
            return redirect(url_for("main.tables", tablename=tablename))
        # 取消更改
        elif form.restore.data:
            g_config.db_status = 5
            db.session.rollback()

    # 删除一条数据，按编号删除
    delete_form = DeleteForm()
    # 提交删除编号后，不再显示删除框
    if delete_form.validate_on_submit() and delete_form.submit.data:
        g_config.delete_no = delete_form.number.data
        g_config.db_status = 3
        if check_idx(g_config.delete_no, 1, len(results)):
            # 准备删除的数据，编号显示为"!!!"
            records[g_config.delete_no-1][0] = "!!!"

    return render_template(
        "tables.html",
        title = "信息查询",
        tablelinks = tablelinks,
        tabletitle = tabletitle,
        atbs = atbs,
        records = records,
        form = form,
        delete_form = delete_form,
        add_forms = add_forms,
        db_status = g_config.db_status
    )
