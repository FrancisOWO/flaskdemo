from flask_wtf import FlaskForm
from wtforms.widgets import NumberInput
from wtforms import StringField, SubmitField, IntegerField

# 删除记录
class DeleteForm(FlaskForm):
    number = IntegerField(
        "删除编号",
        widget=NumberInput(min=1),
        render_kw={
            "class":"form-control",
            "style":"width:70px; height:25px;",
            "placeholder":"0",
        }
    )
    submit = SubmitField(
        "提交",
        render_kw={
            "class": "btn",
            "style": "border:0px; color:blue;",
            }
    )

# 添加记录（输入单个字段）
class ContentForm(FlaskForm):
    content = StringField(
        "输入",
        render_kw={
            "class":"form-control",
            "style":"width:110px; height:30px;",
        }
    )
    submit = None

# 操作数据库表
class TableActForm(FlaskForm):
    add = SubmitField(
        "添加",
        render_kw={
            "class": "btn",
            "style": "border:0px;",
            }
        )
    delete = SubmitField(
        "删除",
        render_kw={
            "class": "btn",
            "style": "border:0px;",
            }
        )
    save = SubmitField(
        "保存",
        render_kw={
            "class": "btn",
            "style": "border:0px;",
            }
        )
    restore = SubmitField(
        "取消",
        render_kw={
            "class": "btn",
            "style": "border:0px;",
            }
        )