from app import db

'''
    # 院系表
    no      院系编号（主键，自增）
    name    院系名称
'''
class Dept(db.Model):
    __tablename__ = "dept"

    no = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(45), nullable=False)


'''
    # 学生表
    no      学号（主键）
    name    姓名
    gender  性别
    age     年龄
    d_no    院系编号（外键，依赖 dept 表）
'''
class Student(db.Model):
    __tablename__ = "student"

    no = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(30), nullable=False)
    gender = db.Column(db.Enum('F','M'), nullable=False)
    age = db.Column(db.Integer, nullable=False)
    d_no = db.Column(db.Integer, db.ForeignKey("dept.no"), nullable=False)


'''
    # 课程表
    no      课程编号（主键，自增）
    name    课程名称
    credit  学分
    d_no    院系编号（外键，依赖 dept 表）
'''
class Course(db.Model):
    __tablename__ = "course"

    no = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(60), nullable=False)
    credit = db.Column(db.Integer, nullable=False)
    d_no = db.Column(db.Integer, db.ForeignKey("dept.no"), nullable=False)


'''
    # 成绩表
    s_no    学生编号（外键，依赖 student 表）
    c_no    课程编号（外键，依赖 course 表）
    score   成绩
    log_dt  登记时间（yyyy-MM-dd HH:mm:ss）
'''
class Score(db.Model):
    __tablename__ = "score"

    s_no = db.Column(db.Integer, db.ForeignKey("student.no"), nullable=False)
    c_no = db.Column(db.Integer, db.ForeignKey("course.no"), nullable=False)
    score = db.Column(db.Integer, nullable=False)
    log_dt = db.Column(db.DateTime, nullable=False)

    # 无主键的表，需构造主键
    __mapper_args__ = {
        "primary_key": [s_no, c_no]
    }
