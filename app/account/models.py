from app import db
from flask_login import UserMixin

'''
    # 用户表
    username    用户名（主键）
    password    密码
'''
class User(UserMixin, db.Model):
    __tablename__ = "user"

    username = db.Column(db.String(20), primary_key=True)
    password = db.Column(db.String(256), nullable=False)
