from flask import render_template, flash, redirect, url_for, session

from app import app, db, g_config, login
from . import account_bp
from .models import User
from .forms import RegisterForm, LoginForm, ChangePwdForm

from werkzeug.security import generate_password_hash, check_password_hash


@login.user_loader
def load_user():
    return

@account_bp.route("/", methods=["GET"])
@account_bp.route("/index", methods=["GET"])
def index():
    return redirect(url_for("account.login"))

@account_bp.route("/login", methods=["GET","POST"])
def login():
    form = LoginForm()
    username = form.username.data
    password = form.password.data
    if form.submit.data:
        user = User.query.get(username)
        if user is None or not check_password_hash(user.password, password):
            flash("昵称或密码错误！", category="danger")
        else:
            session.clear()
            session["username"] = username
            flash("登录成功！", category="info")
            return redirect(url_for("account.panel"))

    return render_template(
        "index.html",
        title = "登录",
        form = form
    )

@account_bp.route("/signup", methods=["GET","POST"])
def signup():
    form = RegisterForm()
    if form.submit.data:
        username = form.username.data
        pwd1 = form.password1.data
        pwd2 = form.password2.data
        user = User.query.get(username)
        if user is not None:
            flash("用户已存在！", category="danger")
        elif pwd1 != pwd2:
            flash("两次密码不一致！", category="danger")
        else:
            user = User(username=username, password=generate_password_hash(pwd1))
            db.session.add(user)
            try:
                db.session.commit()
                flash("注册成功！", category="info")
                return redirect(url_for("account.login"))
            except Exception as e:
                app.logger.info(f"注册失败: {e}")
                db.session.rollback()
                flash(f"注册失败: {e}", category="danger")

    return render_template(
        "signup.html",
        title = "注册",
        form = form
    )

@account_bp.route("/changepwd", methods=["GET","POST"])
def changepwd():
    form = ChangePwdForm()
    if form.submit.data:
        pwd1 = form.password1.data
        pwd2 = form.password2.data
        if pwd1 != pwd2:
            flash("两次密码不一致！", category="danger")
        else:
            # TODO: 密码加密
            try:
                user = User.query.filter(User.username == session["username"]).update({
                    "password": generate_password_hash(pwd1)})
                db.session.commit()
                flash("密码修改成功，请重新登录！", category="info")
                return redirect(url_for("account.login"))
            except Exception as e:
                app.logger.info(f"改密失败: {e}")
                db.session.rollback()
                flash(f"改密失败: {e}", category="danger")

    return render_template(
        "changepwd.html",
        title = "修改密码",
        form = form
    )

@account_bp.route("/logout", methods=["GET"])
def logout():
    session.clear()
    return redirect(url_for("account.login"))

@account_bp.route("/panel", methods=["GET","POST"])
def panel():
    return render_template(
        "panel.html",
        title = "控制面板",
        tablelinks = g_config.tablelinks
    )
