from app import app
from app.account import account_bp
from app.main import main_bp

app.register_blueprint(account_bp)
app.register_blueprint(main_bp)

if __name__ == '__main__':
    # app.run() 为旧版启动方式，建议改用 flask run
    print('Test Address: 127.0.0.1:5000')
    app.run(
        host='127.0.0.1',
        port='5000',
        debug=True
        )
